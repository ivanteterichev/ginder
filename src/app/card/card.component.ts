import { animate, keyframes, transition, trigger, style } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';

@Component({
    selector: 'gndr-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    animations: [
        trigger('swipeCard', [
            transition('* => skip', animate(600, keyframes([
                style({ opacity: 1 }),
                style({ transform: 'translate3d(-200%, 0, 0) rotate3d(0, 0, 1, -90deg)', opacity: 0 }),
            ]))),
            transition('* => favorite', animate(600, keyframes([
                style({ opacity: 1 }),
                style({ transform: 'translate3d(200%, 0, 0) rotate3d(0, 0, 1, 90deg)', opacity: 0 }),
            ])))
        ])
    ]
})
export class CardComponent implements OnInit {
    @Input() swipeDone: Subject<string>;
    @Input() listOfRepositories: any;   // TODO: Add repositoryInterface
    @Input() isStartOfNewSearch: boolean;
    @Output() endOfListReached = new EventEmitter();

    public repositories: any[] = [];
    public index: number = 0;
    public animationState: string;
    private componentDestroyed$ = new Subject<void>;
    public repositoryList: Array<any> = [];

    constructor() { }

    public ngOnChanges(): void {
        this.repositories = this.listOfRepositories;
        if (this.isStartOfNewSearch) {
            this.index = 0;
        }
    }

    public ngOnInit(): void {
        this.swipeDone.pipe(takeUntil(this.componentDestroyed$)).subscribe(event => {
            this.startAnimation(event)
        });
    }

    public startAnimation(state: string): void {
        if (!this.animationState) {
            this.animationState = state;
            this.index++;
        }
    }

    public resetAnimationState(): void {
        this.animationState = '';
        if (this.index === this.repositories.length - 1) {
            this.endOfListReached.emit(true);
        }
    }

    public ngOnDestroy(): void {
        this.componentDestroyed$.next();
        this.componentDestroyed$.complete();
    }
}
