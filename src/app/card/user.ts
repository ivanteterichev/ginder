export interface User {
    id: number;
    name: string;
    image: string;
}

export const data = [
    {
        "id": 0,
        "name": "James Gosling",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 1,
        "name": "Dennis Ritchie",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 2,
        "name": "Bjarne Stroustrup",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 3,
        "name": "Guido van Rossum",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 4,
        "name": "Rasmus Lerdorf",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 5,
        "name": "Larry Wall",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 6,
        "name": "Brendan Eich",
        "image": "https://place-hold.it/500x500"
    },
    {
        "id": 7,
        "name": "Yukihiro Matsumoto",
        "image": "https://place-hold.it/500x500"
    },
];