import { Component } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
    selector: 'gndr-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public title = 'ginder';
    public swipeDone: Subject<string> = new Subject();
    public isEndOfList: boolean;
    public listOfRepositories: any; // TODO: Add repositoryInterface
    public isStartOfNewSearch: boolean;

    constructor() {}

    public endOfListReached(value: boolean) {
        this.isEndOfList = value;
        this.isStartOfNewSearch = false;
    }

    public getListOfRepositories(values: any) {
        this.listOfRepositories = values;
        this.isStartOfNewSearch = true;
    }
}
