import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SearchRepositoryService {
    private readonly repositoryURL = 'https://api.github.com/search/repositories?q=';

    constructor(private http: HttpClient) { }

    getRepositories(query: string): Observable<any> {
        return this.http.get<any>(this.repositoryURL + query);
    }
}
