import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
    selector: 'gndr-button-group',
    templateUrl: './button-group.component.html',
    styleUrls: ['./button-group.component.scss']
})
export class ButtonGroupComponent implements OnInit {
    @Input() swipeDone: Subject<string>;
    @Input() isEndOfList: boolean;
    @Input() isStartOfNewSearch: boolean;

    constructor() { }

    ngOnInit(): void {
    }

    swipe(value: string) {
        this.swipeDone.next(value);
    }
}
