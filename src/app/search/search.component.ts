import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged, EMPTY, filter, map, switchMap } from 'rxjs';
import { SearchRepositoryService } from '../search-repository.service';

@Component({
    selector: 'gndr-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
    @Output() listOfRepositoriesReceived = new EventEmitter<any>;
    public form: FormGroup;

    constructor(private serchRepositoryService: SearchRepositoryService) { }

    public ngOnInit(): void {
        this.initForm();
        this.getRepositories();
    }

    public initForm(): void {
        this.form = new FormGroup({
            searchRepositories: new FormControl(''),
        });
    }

    public getRepositories(): void {
        this.form.controls.searchRepositories.valueChanges
            .pipe(
                debounceTime(1000),
                distinctUntilChanged(),
                filter(query => query.trim()),
                switchMap(query => this.serchRepositoryService.getRepositories(query)
                    .pipe(
                        catchError(error => EMPTY)
                    )),
                map(response => response.items.slice(20))
            )
            .subscribe((repositories) => {
                this.listOfRepositoriesReceived.emit(repositories);
            })
    }
}
